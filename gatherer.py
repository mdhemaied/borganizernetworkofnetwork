from threading import Thread




class Gatherer(Thread):

	GATHERERSTATES = [
		'Waiting for Target',
		'Gathering Intel',
	]

	def __init__(self):
		Thread.__init__(self)

		self.set_status(0)
		self.target = None
		self.intel = {}

	def set_status(self,s):
		self.status_code = s
		self.status = self.GATHERERSTATES[s]


	def get_report(self):
		if self.intel is None:
			raise Exception('No intel to report !')
		return self.intel


	def get_status(self):
		return (self.status,self.status_code)


	def gather(self,target):
		self.target = target
		self.run()


	def run(self):
		self.set_status(1)
		self.gather_info()
		self.set_status(0)




