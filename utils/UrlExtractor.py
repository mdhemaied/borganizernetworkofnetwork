from urlextract import URLExtract
from urllib.parse import urlparse,unquote
from collections import Counter


SOCIAL_MEDIAS = [
	'facebook',
	'twitter',
	'instagram',
	'youtube',
	'plus.google',
]


SOCIAL_MEDIAS_DICT = {
	'facebook':'facebook',
	'twitter':'twitter',
	'instagram':'instagram',
	'youtube':'youtube',
	'plus.google':'google+',
}



def ParseUrl(url):
	if '//' in  url:
		return urlparse(url)
	else:
		return urlparse("http://"+url)


def QueryArgs(query):
	cps = query.split('&')
	return [c.split('=') for c in cps]


def UnquoteUrl(url):
	return unquote(url)



def NormalizeUrl(u):

	if u.startswith('www.'):
		url = u.replace('www.','')
	else:
		url = u.replace('//www.','//')

	if '//' in  url:
		return UnquoteUrl(''.join(urlparse(url)[1:]))
	else:
		return UnquoteUrl(url)






def UrlsFromHtml(html,transform=False,base_url=''):

	if transform and base_url is '':
		raise Exception('Can\'t transform urls without the base_url')

	urls = set()

	encore = True
	last_pos = 0

	while encore:

			indx = html.find('href',last_pos)
			if indx >= 0: 
				indx1 = html.find('"',indx)
				indx2 = html.find('"',indx1+1)

				if transform:
					up = ParseUrl(html[indx1+1:indx2])
					if up[1] is '':
						urls.add(NormalizeUrl(base_url+html[indx1+1:indx2]))
					else:
						params = '?'+up[4]+''.join(up[5:]) if up[4] != '' else ''
						urls.add(NormalizeUrl(up[1].split(':')[0]+''.join(up[2:3])+params))
				else:
					urls.add(NormalizeUrl(html[indx1+1:indx2]))

				last_pos = indx2
			else:
				encore = False

	return list(urls)



def Counted_UrlsFromHtml(html,transform=False,base_url=''):

	if transform and base_url is '':
		raise Exception('Can\'t transform urls without the base_url')

	urls = []

	encore = True
	last_pos = 0

	while encore:

			indx = html.find('href',last_pos)
			if indx >= 0: 
				indx1 = html.find('"',indx)
				indx2 = html.find('"',indx1+1)

				if transform:
					up = ParseUrl(html[indx1+1:indx2])
					if up[1] is '':
						urls.append(NormalizeUrl(base_url+html[indx1+1:indx2]))
					else:
						params = '?'+up[4]+''.join(up[5:]) if up[4] != '' else ''
						urls.append(NormalizeUrl(up[1].split(':')[0]+''.join(up[2:3])+params))
				else:
					urls.append(NormalizeUrl(html[indx1+1:indx2]))

				last_pos = indx2
			else:
				encore = False

	return Counter(urls)








def LocateUrlsInText(text):
	urls = []
	ext = URLExtract()
	try:
		urls = ext.find_urls(str(text))
	except Exception as exc:
		pass

	return [NormalizeUrl(u) for u in urls]


def LocateMailsInText(text):
	return []






def isSocialMedia(url):
	for sm in SOCIAL_MEDIAS:
		if sm in ParseUrl(url)[1]+'/':
			return True
	return False


def getSocialMedia(url):

	for sm in SOCIAL_MEDIAS_DICT:
		if sm in ParseUrl(url)[1]+'/':
			return SOCIAL_MEDIAS_DICT[sm]
	return ''



