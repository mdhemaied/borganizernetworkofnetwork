##
# Permet de controler toute la chaine gather -> confirm
from .Borganizer import Borganizer
from .utils.CsvFiles import dictTocsv

from collections import Counter


import wikipedia as wiki
import yaml,os,time

main_target = 'List_of_Malay_people'

categories = ['Activists', 'Astronomy', 'Astronauts', 'Business', 'Chefs', 'Designers', 'Entertainers', 'Leaders', 'Literary figures', 'Politicians', 'Royalty', 'Sportspeople', 'Visual artists', 'Warriors', 'References', 'See also']

if __name__ == '__main__':


	WP = wiki.WikipediaPage(main_target)
	Borg = Borganizer()

	section = WP.content.split('== '+categories[0]+' ==')[1]
	sections = []

	for i in range(len(categories)-2):
		targets = section.split('== '+categories[i+1]+' ==')[0].split('\n')
		sections.extend([{'tags':categories[i],'name':t} for t in targets])
		section = section.split('== '+categories[i+1]+' ==')[1]


	dct = {
		'name':[],
		'tags':[],
		'facebook':[],
		'twitter':[],
		'instagram':[],
		'googleplus':[],
		'youtube':[],
		'websites':[]
	}
	

	for target in sections:

		name = target['name']
		tags = target['tags']

		if len(name) > 1:

			while not name[0].isalpha():
				name = name[1:]

			while Counter([(char.isalpha() or char in [' ','.','\'']) for char in name])[False] > 0:
					name = name[:-1]

			while not name[-1].isalpha():
				name = name[:-1]

			appended = 0
			target['name'] = name

			print('Takin care of '+name+'...')
			reports = Borg.get_links_of({'name':name})
			print('Took care of '+name+'...')

			directory = './Reports/'+name.replace(' ','_')
			for report in reports:
				if not os.path.exists(directory):
		   			os.makedirs(directory)
				open(directory+'/'+report+'.yaml','w').write(yaml.dump(reports[report],default_flow_style=False))

			print('Saved '+name+'...')

			dct['name'].append(name)
			dct['tags'].append(tags)

			for site in reports['Borganizer']['picked_urls']:
				if site in dct.keys():
					dct[site].append(' , '.join([l for l in reports['Borganizer']['picked_urls'][site]]))


	directory = './Reports/Csv'
	if not os.path.exists(directory):
		os.makedirs(directory)

	print(dct)
	tm = str(time.time())
	open(directory+'/reportcsv_'+tm+'.yaml','w').write(yaml.dump(dct,default_flow_style=False))
	dictTocsv(dct,directory+'/'+tm+'.csv')
