import unicodecsv as csv

def dictTocsv(dct,targetfile):

	with open(targetfile, 'wb') as csvfile:
		fieldnames = dct.keys()
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

		writer.writeheader()
		for i in range(max([len(dct[d]) for d in dct])):
			to_add = {}
			for field in fieldnames:
				try:
					to_add[field] = dct[field][i]
				except:
					pass
			writer.writerow(to_add)
		print(str(i)+' rows printed')