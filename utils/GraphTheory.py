

class SimpleGraph():

	"""docstring for Graph"""
	def __init__(self):
		self.last_node_id = 1
		self.nodes = {}
		self.links = {}


	def create_init_node(self,attrs={}):

		if 0 in self.nodes:
			raise Exception('Init Node already created.')

		self.nodes[0] = attrs

		return 0


	def create_node(self,attrs):

		self.nodes[self.last_node_id] = attrs
		self.last_node_id += 1

		return self.last_node_id-1


	def link_nodes(self,node1,node2,cost=0):
		if cost >= 0:
			self.links['+'+str(node1)+'|'+str(node2)+'+'] = cost
		else:
			raise Exception('Ras zeebi ne peut pas etre negatif')



	def nodes_ByAttrs(self,searched_attrs):
		nodes = []

		for i in range(self.last_node_id):
			cor = True
			for s in searched_attrs:
				try:
					cor = cor and (searched_attrs[s] == self.nodes[i][s])
				except:
					cor = False

			if cor:
				nodes.append(i)

		return nodes


	def route_MinCost(self,node_start,node_end):

		if not (node_start in self.nodes and node_end in self.nodes):
			raise Exception('One node isn\'t in The node')

		costs = {n:'inf' for n in self.nodes}
		costs[node_start] = 0

		done = []
		current_node = node_start

		while not node_end in done and not current_node is None:

			for l in self.links:

				if '+'+str(current_node)+'|' in l:
					node = int(l.replace('+'+str(current_node)+'|','').replace('+',''))
					cost = self.links[l]

					if (costs[node] == 'inf') or (costs[node] > costs[current_node]+cost):
						costs[node] = costs[current_node]+cost

			done.append(current_node)
			current_node = None
			for n in self.nodes:
				if not (n in done or costs[n] is 'inf'):
					if current_node is None:
						current_node = n
					elif costs[current_node] > costs[n]:
						current_node = n

		return costs[node_end]
