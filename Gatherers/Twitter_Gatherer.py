from ..gatherer import Gatherer
from ..utils.UrlExtractor import *
from collections import Counter

import time as tm
import tweepy
import sys




class Twitter_Gatherer(Gatherer):

	twitter_CONSUMER_KEY = 'wy7bGnAOFvN9Pqbb515w0bViM'
	twitter_CONSUMER_SECRET = 'stEoKuWPgEPwUdVKrkeZSKUsvLLXU86ZTbbOoRPHsEq2tIfPal'
	twitter_ACCESS_TOKEN = '889465371593408512-EY18MG169xqs9bvPjoLD5693EmMLjQJ'
	twitter_ACCESS_TOKEN_SECRET = 'rAqeCRt6WOzfRXAFcvMXsu4gT9AoyiDA3aevxMSd1fxLs'

	TWITTER_FIELDS = ['id_str','verified','url','entities','description','name','lang','followers_count','location','id']
	TRANSLATED_METADATA_FIELDS = {
		'followers_count':'followers',
		'location':'country',
		'name':'title',
	}

	"""docstring for Twitter_Gatherer"""
	def __init__(self):
		Gatherer.__init__(self)

		auth = tweepy.OAuthHandler(self.twitter_CONSUMER_KEY, self.twitter_CONSUMER_SECRET)
		auth.set_access_token(self.twitter_ACCESS_TOKEN, self.twitter_ACCESS_TOKEN_SECRET)
		self.twitter_api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)


	def gather_info(self):

		print('Searching for '+str(self.target)+' on twitter ...')
		user = self.twitter_api.search_users(self.target)[0]
		
		self.intel = {
			'target':self.target,
			'link':NormalizeUrl('twitter.com/'+user.__dict__['id_str']),
			'verified':user.__dict__['verified'],
			'sm_links':[],
			'metadata':{
				'external_links':[],
			}
		}


		for field in self.TWITTER_FIELDS[2:]:
			for url in LocateUrlsInText(str(user.__dict__[field])):
				if url != self.intel['link']:
					if isSocialMedia(url):
						self.intel['sm_links'].append(url)
					else:
						self.intel['metadata']['external_links'].append(url)
			try:
				self.intel['metadata'][self.TRANSLATED_METADATA_FIELDS[field]] = user.__dict__[field]
			except:
				pass

		counted = Counter(self.intel['sm_links'])
		self.intel['sm_links'] = [{'url':u,'count':counted[u]} for u in counted]

		counted = Counter(self.intel['metadata']['external_links'])
		self.intel['metadata']['external_links'] = [{'url':u,'count':counted[u]} for u in counted]



		
if __name__ == '__main__':

	import yaml

	WG = Twitter_Gatherer()
	t = tm.time()

	WG.gather(sys.argv[1])
	open('./Tests/TWITTER_Report_'+sys.argv[1].replace(' ','_')+'.yaml','w').write(yaml.dump(WG.get_report(),default_flow_style=False))
	print(str(int(tm.time()-t))+'s')
