## Imports
from bs4 import BeautifulSoup as bs
from ..targeter import Targeter
from collections import Counter
from copy import deepcopy

import os
import time
import yaml
import requests



TEMPLATE_FACEBOOK_NODE = {
	'id':'',
	'name':'',
	'selected_fans':[],
	'metadata':{
		'fan_count':-1,
		'category':'',
		'is_verified':False
	}
}

CATEGORIES_DICT = None


def Translator_tostr(s):
	return str(s)

def Translator_tolower(s):
	return s.lower()

def Translater_unspace(s):
	return s.replace(' ','_')

def Translater_space(s):
	return s.replace('_',' ')

def Translater_CategoryTransform(s):
	try:
		return CATEGORIES_DICT[s]
	except:
		return 'none'


ESC_CHAR = '%'
FACEBOOK_DICT_TO_NODE_TRANSLATOR = {
	'id':([Translator_tostr],'id'),
	'name':(None,'name'),
	'is_verified':([Translator_tostr],'metadata'+ESC_CHAR+'is_verified'),
	'fan_count':(None,'metadata'+ESC_CHAR+'fan_count'),
	'category':([Translator_tolower,Translater_CategoryTransform,Translater_unspace],'metadata'+ESC_CHAR+'category')
}


########################################################################################################
# Filters give direction to facebook targeter
# A filter is a couple of a comparative condition and a basic or complexe exclusionnary condition :
# # A comparative condition is {key: operator} :
# # # the key must be one of the 'metadata' dict keys of a facebook node (of type int)
# # # the operator can be:
# # # # '>' for highest
# # # # '<' for lowest
# # # # '~ int' for closest to int
# # An exclusionnary condition can be basic or complexe :
# # # Basic exclusionnay condition is similar to the comparative one {key: operator}:
# # # # the key must be one of the 'metadata' dict keys of a facebook node (of type int)
# # # # the operator can be:
# # # # # '= value' for equal value(exclusionnary)
# # # # # '!= value' for not equal value (exclusionnary)
# # # Complexe exclusionnay condition is a composition of different basic or complexes conditions:
# # # # (Condition1, Condition2, ... ) is equivalent to Condition1 OR Condition2 OR ....
# # # # [Condition1, Condition2, ... ] is equivalent to Condition1 AND Condition2 AND ....
########################################################################################################
# Example :
# Filter that only keeps pages with category 'politics' or 'aquatic sports' and prioritizes the highest fan_count page
# Filter = ('fan_count':'>',({'category':'= politics'},({'category':'= aquatic_sports'})))
COMPARATIVE_OPERATORS = ['>','<','~']
EXCLUSIONNARY_OPERATORS = ['=','!=']
DEFAULT_FILTER = (True,True)


DEFAULT_FIELDS_FANS_DATA = 'likes{id,about,bio,username,website,name,link,fan_count,is_verified,category,talking_about_count}'
DEFAULT_FIELDS_DATA = 'fan_count,category,is_verified,name'



NUMBER_OF_STORED_FANS = 25
INDEXES=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']


DEFAULT_ACCESS_TOKEN = '132561900596439|876Lz2TPUZ2a6BDhn2PbvRzlVX0'
		


class Facebook_Targeter(Targeter):
	"""Subclass of Targeter
	This class is designed to search targets on facebook.
	It uses an algorithms based on a Network of Network search.
	As explained in details later, this class can implement a filter to search in a precise direction.
	"""

	def __init__(self,cnf,nbsf=NUMBER_OF_STORED_FANS,
		at=DEFAULT_ACCESS_TOKEN):

		Targeter.__init__(self)
		self.access_token = at
		self.nb_stored_fans = nbsf

		self.COSMOS = deepcopy(TEMPLATE_FACEBOOK_NODE)
		self.COSMOS['id'] = '-1'

		self.todo_list = []

		self.pagefansfields = DEFAULT_FIELDS_FANS_DATA
		self.pagefields = DEFAULT_FIELDS_DATA

		if os.isfile(cnf):
			self.configs = yaml.load(open(cnf,'r').read()).
		else:
			raise Exception('Facebook Targeter: Wrong configs file path')

		try:
			if os.isfile(self.configs['categories_file']):
				self.known_categories = yaml.load(open(self.configs['categories_file'],'r').read())
			else:
				raise Exception()
		except:
			raise Exception('Facebook Targeter: Wrong categories file path')


		CATEGORIES_DICT = self.known_categories



	def create_node(self,id_):

		n = deepcopy(self.template_node)
		n['id'] = str(id_)

		return n



	def FBdict_to_node(self,dct):

		new_dict = deepcopy(self.template_node)

		for src,dst in self.FB_toNode_translator.items():
				ops = dst[0]
				trg = dst[1]

				# transform data
				data = dct[src]
				if not ops is None:
					for op in ops:
						data = op(data)
				
				# store data
				dsts = trg.split(ESC_CHAR)
				pos = new_dict
				i = 1
				for d in dsts[:-1]:
					pos = pos[d]
					i+=1
				pos[dsts[-1]] = data

		return new_dict



	def get_node_ByPath(self,path):
		current_node = self.COSMOS
		for p in path:
			current_node = current_node['selected_fans'][INDEXES.index(p)]

		return current_node



	def get_node_ByID(self,id_,node=None):
		if node is None:
			node = self.COSMOS

		if node['id'] == str(id_):
			return node

		for fan in node['selected_fans']:
			rslt = self.get_node_ByID(str(id_),node=fan)
			if not rslt is None:
				return rslt

		return None


	def CheckExclusionnaryCondition(self,condition):

		if condition is True:
			return True
		elif type(condition) is type([]) or type(condition) is type(()):
			for c in condition:
				self.CheckExclusionnaryCondition(c)
		elif type(condition) is type({}):
			if len(condition.keys()) != 1:
				raise Exception('Filter Exception: A Basic condition must contain only one set of key: operator.')

			if not list(condition.keys())[0] in TEMPLATE_FACEBOOK_NODE['metadata']:
				raise Exception('Filter Exception: Unknown field '+str(condition.keys()[0])+'.')

			fragments = condition[list(condition.keys())[0]].split(' ')
			if len(fragments) != 2:
				raise Exception('Filter Exception: Wrong syntaxe '+str(condition[list(condition.keys())[0]])+'.')

			operator = fragments[0]
			if not operator in EXCLUSIONNARY_OPERATORS:
				raise Exception('Filter Exception: Wrong operator '+str(operator)+'.')
		else:
			raise Exception('Filter Exception: Unknown condition type.')



	def CheckFilter(self,filter_):

		if not type(filter_) is type(()):
			if not len(filter_) == 2:
				raise Exception('Filter Exception: A filter is a tuple of 2 conditions, the first comarative and the second exclusionnary.')

		# Can contain only one valid comparative condition

		if not filter_[0] is True:
			if not type(filter_[0]) is type({}):
				raise Exception('Filter Exception: The comparative condition must be a dict containing a single key')

			if len(filter_[0].keys()) != 1:
				raise Exception('Filter Exception: A filter mlust contain one and only one comparative condition.')

			if not list(filter_[0].keys())[0] in TEMPLATE_FACEBOOK_NODE['metadata']:
				raise Exception('Filter Exception: Unknown field '+str(filter_[0].keys()[0])+'.')

			if not type(TEMPLATE_FACEBOOK_NODE['metadata'][list(filter_[0].keys())[0]]) is type(0):
				raise Exception('Filter Exception: A comparative condition must be applyed to an integer field.')

			if not filter_[0][list(filter_[0].keys())[0]].split(' ')[0] in COMPARATIVE_OPERATORS:
				raise Exception('Filter Exception: Wrong comparative operator '+str(filter_[0][list(filter_[0].keys())[0]].split(' ')[0])+'.')

		# Check if exclusionnary fields exists:
		self.CheckExclusionnaryCondition(filter_[1]) 

		return True



	def SetupFilter(self,filter_):

		print('Compiling Filter ...')
		self.CheckFilter(filter_)

		# Remove untested fields
		self.template_node = TEMPLATE_FACEBOOK_NODE
		self.FB_toNode_translator = FACEBOOK_DICT_TO_NODE_TRANSLATOR

		self.filter = filter_



	def ApplyExclusionnaryFilter(self,node,condition=None):

		if condition is None:
			condition = self.filter[1]

		if condition is True:
			rslt = True

		elif type(condition) is type(()):
			rslt = False
			for cond in condition:
				rslt = rslt or self.ApplyExclusionnaryFilter(node,condition=cond)
				if rslt is True:
					break

		elif type(condition) is type([]):
			rslt = True
			for cond in condition:
				rslt = rslt and self.ApplyExclusionnaryFilter(node,condition=cond)
				if rslt is False:
					break

		elif type(condition) is type({}):
			field = list(condition.keys())[0]
			operator = condition[field].split(' ')[0]
			valor = condition[field].split(' ')[1]
			if operator is '=':
				rslt = node['metadata'][field] == valor
			elif operator is '!=':
				rslt = not node['metadata'][field] != valor

		return rslt





	def FetchFans_ByFilter(self,path):

		target = self.get_node_ByPath(path)
		print('Fetching realted pages to '+str(target['name']))
		id_ = target['id']
		done = False
		while not done:
			try:
				L=requests.get('https://graph.facebook.com/v2.9/'+str(int(id_))+'?fields='+self.pagefansfields+'&access_token='+self.access_token).json()
				done = True
			except:
				time.sleep(5)

		try:
			new_targets = []
			for new_target in L["likes"]["data"] :

				# Save Category If need to
				try:
					if not new_target['category'].lower() in self.known_categories:
						self.known_categories[new_target['category'].lower()] = 'unclassified'
						open(self.configs['categories_file'],'w').write(yaml.dump(self.known_categories,default_flow_style=False))
						print('Added Category: '+new_target['category'].lower())
				except Exception as exc:
					print(exc)


				if len(new_targets) > self.nb_stored_fans:
					break

				new_target_node = self.FBdict_to_node(new_target)
				if self.ApplyExclusionnaryFilter(new_target_node):
					new_targets.append(deepcopy(new_target_node))

			for new_target in new_targets:
				if self.get_node_ByID(new_target['id']) is None:
					target['selected_fans'].append(deepcopy(new_target))
					self.todo_list.append(path+INDEXES[len(target['selected_fans'])-1])
		except:
			pass



	def update_PageData(self,path):

		target = self.get_node_ByPath(path)
		id_ = target['id']
		done = False
		while not done:
			try:
				L=requests.get('https://graph.facebook.com/v2.9/'+str(int(id_))+'?fields='+self.pagefields+'&access_token='+self.access_token).json()
				done = True
			except:
				time.sleep(5)

		new_target = self.FBdict_to_node(L)
		for key in target:
			target[key] = new_target[key]



	def update_TodoList(self):

		# Sort The targets list
		print('Updating Targets list ...')
		comparative = self.filter[0]

		if not comparative is True:

			field = list(comparative.keys())[0]
			op = comparative[field].split(' ')[0]
			val = int(comparative[field].split(' ')[1]) if ' ' in comparative else None

			filter_reverse = False
			if op is '>':
				filter_reverse = True

			filtre_func = lambda x:self.get_node_ByPath(x)['metadata'][field]
			if op is '~':
				filtre_func = lambda x:abs(self.get_node_ByPath(x)['metadata'][field]-val)

			self.todo_list = sorted(self.todo_list,key=filtre_func,reverse=filter_reverse)



	## Function: Get Source
	def getNextTarget(self):

		# Query last target's fans page data
		if not self.last_target is None:
			self.FetchFans_ByFilter(self.last_target)

		# Select the next to be targeted
		self.update_TodoList()
		if len(self.todo_list) > 0:
			self.last_target = self.todo_list.pop(0)
			return self.get_node_ByPath(self.last_target)

		return None



	def init_WithTargetsID(self,targets,filter=DEFAULT_FILTER):

		print('initializing the targeter with fb_ids list ...')
		self.SetupFilter(filter)

		if len(targets) < self.nb_stored_fans:

			self.COSMOS['selected_fans'] = [self.create_node(target) for target in targets]
			self.todo_list = [INDEXES[i] for i in range(len(targets))]

			print('Quering Facebook data for the fb_ids list ...')
			for todo in self.todo_list:
				self.update_PageData(todo)

			self.last_target = None

		else:
			raise Exception('Target\'s list longer than authorized. Targeter initialization failed.')

		return True



	def init_WithDict(self,dct,filter=DEFAULT_FILTER):

		print('initializing the targeter with dict ...')
		self.SetupFilter(filter)

		raise Exception('Target\'s list longer than authorized. Targeter initialization failed.')

		return True



## Main (Fot Tests)
if __name__ == '__main__':
	FBT = Facebook_Targeter()
	FBT.init_WithTargetsID(['92023490653'],filter=({'fan_count':'>'},[{'is_verified':'= True'},{'category':'= animals_&_pets'}]))
	limit = 20
	i = 0
	while(i < limit):

		time.sleep(2)
		nxt = FBT.getNextTarget()
		print('\nChosen Target : '+str(nxt['id']+' - '+str(nxt['name'])))
