def PureText(txt):
	text = []
	for t in txt:
		if t.isalpha():
			text.append(t)
	return ''.join(text)


def GetNumber(txt,sep='.'):
	text = []

	i = 0
	try:
		while not (txt[i].isnumeric() or txt[i] == sep):
			i += 1

		while (txt[i].isnumeric() or txt[i] == sep):
			text.append(txt[i])
			i += 1

	except Exception as exc:
		print(exc)

	return ''.join(text)