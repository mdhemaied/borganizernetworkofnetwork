from ..gatherer import Gatherer
from ..utils.UrlExtractor import LocateUrlsInText
from collections import Counter

from instagram.client import InstagramAPI



import time as tm
import sys


class Instagram_Gatherer(Gatherer):
	"""docstring for Youtube_Gatherer"""


	access_token = "3123381139.366fffd.5be62d70b0ca4b998ed5210cd1e6a351"
	client_secret = "eae8e18827244476bb895bdfe1a7061d"

	SEARCH_LIMIT = 5

	def __init__(self, ):
		Gatherer.__init__(self)
		self.api = InstagramAPI(access_token=self.access_token, client_secret=self.client_secret)


		
	def gather_info(self):

		print('Searching for '+self.target+' on Instagram...')
		users = self.api.user_search(self.target)[0:self.SEARCH_LIMIT]

		matches = {}
		for user in users:
			matches[user.__dict__['username']] = self.api.user(user.__dict__['id'])

		#users = sorted(users,
		#			key=lambda x:matches[x.username].__dict__['counts']['followed_by'],
		#			reverse=True
		#		)

		best_match = matches[users[0].username]

		urls = []

		self.intel = {
			'target':self.target,
			'link':'www.instagram.com/'+best_match.username,
			'verified':False,
			'sm_links':urls,
			'metadata':{}
		}

		for field in best_match.__dict__:
			self.intel['sm_links'].extend(LocateUrlsInText(best_match.__dict__[field]))
			self.intel['metadata'][field] = best_match.__dict__[field]

		counted = Counter(self.intel['sm_links'])
		self.intel['sm_links'] = [{'url':u,'count':counted[u]} for u in counted]




		
if __name__ == '__main__':

	WG = Instagram_Gatherer()

	t = tm.time()

	WG.gather(sys.argv[1])
	open('./Reports/INSTAGRAM_Report_'+sys.argv[1].replace(' ','_')+'.yaml','w').write(WG.get_report())
	print(str(int(tm.time()-t))+'s')


		
			