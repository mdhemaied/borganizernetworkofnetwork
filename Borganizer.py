from .Gatherers.Facebook_Gatherer import Facebook_Gatherer
from .Gatherers.Instagram_Gatherer import Instagram_Gatherer
from .Gatherers.Twitter_Gatherer import Twitter_Gatherer
from .Gatherers.Wikipedia_Gatherer import Wikipedia_Gatherer
from .Gatherers.Youtube_Gatherer import Youtube_Gatherer


from .utils.UrlExtractor import isSocialMedia,getSocialMedia
from .utils.GraphTheory import SimpleGraph

import os,sys,time as tm
import yaml


Report_Template = {
	'target': '',									#-- Target name

	'link': '',										#-- Link on The Target Website or SM
	'verified': False,								#-- Is the Link Verified ?

	'sm_links': [ 										#-- Social media Links
		{
			'url' : '',									#--- Link
			'count' : 0,									#--- Link Nb Apparitions
		}
	],

	'metadata':{										#-- Any other Data Obtained

		'title': '',									#--- Name of Person, place, event, ....
		'description': '',								#--- description of the target

		'external_links': [									#--- Any link (Not Social Media) That Appeared
			{
				'url' : '',								#---- Link
				'count' : 0,									#---- Link Nb Apparitions
			}
		],

		'country': '',									#--- Country of the target
		'lang': '',										#--- Language used by the target
		
		'followers': 0,									#--- Followers of the target
		'following': 0,									#--- People followed by the target
		'created_at': '',									#--- Creation Date of the page

		'recommandations':{									#--- Recommandations
			'target': '',									#---- target's name
			'link': '',										#---- target's link
		},

		'misc':{}										#--- Dict of attributes (can be filled with anything)
	}
}




Gatherers__ = {
	'Youtube_Gatherer':Youtube_Gatherer,
	'Facebook_Gatherer':Facebook_Gatherer,
	'Instagram_Gatherer':Instagram_Gatherer,
	'Wikipedia_Gatherer':Wikipedia_Gatherer,
	'Twitter_Gatherer':Twitter_Gatherer,
}

TargetedMedias__ = [
	'facebook',
	'twitter',
	'instagram',
	'youtube',
	'google+',
	'website',
]






class ReportsGraph(SimpleGraph):

	"""docstring for ReportsGraph"""
	def __init__(self):
		SimpleGraph.__init__(self)


	def linkType(self,lnk):

		if isSocialMedia(lnk):
			rtype = getSocialMedia(lnk)
		else:
			rtype = 'website'

		return rtype



	def ReportToNode(self,parent_node,report,cost=10):

		try:
			title = report['metadata']['title']
		except:
			title = ''

		

		node = self.create_node({
			'title':title,
			'type':self.linkType(report['link']),
			'level':self.nodes[parent_node]['level']+1,
			'url':report['link']
		})

		self.link_nodes(parent_node,node,cost=cost)

		for link in [sm['url'] for sm in report['sm_links']]:

			search = self.nodes_ByAttrs({'url':link})

			if len(search) == 0:

				fnode = self.create_node({
					'title':title,
					'type':self.linkType(link),
					'level':self.nodes[node]['level']+1,
					'url':link
				})

				self.link_nodes(node,fnode,cost=10)

			else:

				self.link_nodes(node,search[0],cost=0)

		return node


	







class Borganizer(object):


	"""docstring for Borganizer"""
	BORGANIZER_STATES = [
		'Waiting for gatherers',
		'Regrouping Intel',
		'Waiting'
	]


	BORGANIZER_SEARCH_METHODS = 0
	BORGANIZER_EXPAND_METHODS = 1





	def search_for_target(self,name):

		instances = {}
		dead_instances = []


		# Instanciate Gatherer Objects
		for gatherer in Gatherers__:
			instances[gatherer] = Gatherers__[gatherer]()

		# Launch the gathering
		print('Waiting for results on '+str(name)+'...')
		for instance in instances:
			try:
				instances[instance].gather(name)
			except Exception as exc:
				dead_instances.append(instance)
				print(instance+' is dead')
				print(exc)

		# Waits for completition
		waiting = True
		while waiting:
			waiting = False
			tm.sleep(4)
			for instance in instances:
				if not instance in dead_instances:
					waiting = (waiting) or (instances[instance].get_status()[1] == 1)


		# Remove dead instances 
		for dead in dead_instances:
			instances.pop(dead)


		return {i:instances[i].get_report() for i in instances}





	def get_links_of(self,target,method=0,digging_level=1):

		# Init
		print('\n----------------------------------------------------------------\n')
		print('Initializing the borganizer...')
		name = target['name']
		target_type = target['type'] if 'type' in target else ''
		target_link = target['link'] if 'link' in target else ''

		if method in [self.BORGANIZER_EXPAND_METHODS] and target_link is '':
			raise Exception('Can\'t use expand method without a starting link')



		# Links Graph
		graph = ReportsGraph()

		# Choose gathering method
		if method == self.BORGANIZER_SEARCH_METHODS:

			print('Searching for target ...')
			target_node = graph.create_init_node(attrs={'title':name,'type':target_type,'level':0})
			reports = self.search_for_target(name)
			lvl = 1
			last_added_nodes = []

			for n,r in reports.items():

				confidence = 10
				if not r['verified']:
					confidence = 50

				last_added_nodes.append(graph.ReportToNode(target_node,r,cost=confidence))

			print('Digging the internet (lvl = '+str(lvl)+')...')


		elif method == self.BORGANIZER_EXPAND_METHODS:

			print('Expanding knowledge about target ...')
			gatherer = Gatherers__[target_link['Gatherer']]()
			target_link.pop('Gatherer')
			gatherer.gather()



		# Merges the reports
		print('Borganizing the reports ...')

		big_report = {
			'target':target,
			'sm_links':{t:'' for t in TargetedMedias__},
		}

		for sm in TargetedMedias__:

			nodes = graph.nodes_ByAttrs({'type':sm})
			if len(nodes):
				costs = [graph.route_MinCost(target_node,node) for node in nodes]
				less_cost_nodes = []

				for i in range(len(costs)):
					if costs[i] == min(costs):
						less_cost_nodes.append(nodes[i])

				big_report['sm_links'][sm] = [graph.nodes[node]['url'] for node in less_cost_nodes]


		print('Borganized !')
		print('\n________________________________________________________________\n')

		return big_report





		
if __name__ == '__main__':

	WG = Borganizer()

	t = tm.time()

	report = WG.get_links_of({"name":sys.argv[1],'type':'person'})
	directory = './Reports/'+sys.argv[1].replace(' ','_')
	if not os.path.exists(directory):
   		os.makedirs(directory)
	open(directory+'/Borganizer_Report.yaml','w').write(yaml.dump(report,default_flow_style=False))

	print(str(int(tm.time()-t))+'s')