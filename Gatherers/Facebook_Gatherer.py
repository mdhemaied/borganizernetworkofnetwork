from ..gatherer import Gatherer
from ..utils.UrlExtractor import LocateUrlsInText
from collections import Counter

import facebook as fb
import time as tm
import sys




class Facebook_Gatherer(Gatherer):

	FACEBOOK_TOKEN = "132561900596439|876Lz2TPUZ2a6BDhn2PbvRzlVX0"
	FIELDS = 'id,name,about,birthday,hometown,is_verified,website,bio'
	SEARCH_LIMIT = 1

	
	def __init__(self):
		Gatherer.__init__(self)
		self.graph = fb.GraphAPI(access_token=self.FACEBOOK_TOKEN, version="2.7")


	def gather_info(self):
		#Try to find the page

		print('Searching for '+self.target+' on Facebook...')
		pages = self.graph.request(self.graph.version+'/search',{'q':self.target,'type':"page"})

		i = 0
		while i < self.SEARCH_LIMIT:

			page_id = pages['data'][i]['id']
			page = self.graph.get_object(id=page_id,fields=self.FIELDS)
			urls = []

			for field in page:
				urls.extend(LocateUrlsInText(page[field]))

			self.intel = {
				'target':self.target,
				'link':'www.facebook.com/'+str(page_id),
				'sm_links':urls,
				'verified':page['is_verified'],
				'metadata':{}
			}

			counted = Counter(self.intel['sm_links'])
			self.intel['sm_links'] = [{'url':u,'count':counted[u]} for u in counted]

			i += 1


if __name__ == '__main__':

	WG = Facebook_Gatherer()

	t = tm.time()

	WG.gather(sys.argv[1])
	open('./Tests/FACEBOOK_Report_'+sys.argv[1].replace(' ','_')+'.yaml','w').write(WG.get_report())
	print(str(int(tm.time()-t))+'s')





