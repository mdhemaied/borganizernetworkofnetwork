from ..gatherer import Gatherer
from ..utils.UrlExtractor import *

from bs4 import BeautifulSoup
import wikipedia as wiki
import time as tm,sys

from urllib.parse import urlparse




class Wikipedia_Gatherer(Gatherer):

	def __init__(self):
		Gatherer.__init__(self)



	def wikiUrlScope(self,link):

		pr = ParseUrl(link)
		score = -1
		target = ''

		if 'wikipedia' in pr[1].lower():
			if not ':' in pr[2] and '/wiki/' in pr[2]:
				return {
					'score':0,
					'target':pr[2].replace('/wiki/','').replace('_',' ')
				}
		else:

			for word in self.target.split(' '):
				if word.lower() in pr[1].lower():
					score += 2

			if score == -1:
				for word in self.target.split(' '):
					if word.lower() in pr[1].lower():
						score = 0
						break
		return {
			'score':score,
		}




	def gather_info(self):

		# Try to get the page directly
		try:
			print('Accessing directly '+self.target+'\'s Wikipedia Page...')
			page = pageOf(self.target)
		except Exception as exc:
			print(exc)
			print('Trying to search for '+self.target+'\'s Wikipedia Page...')
			page = searchPage(self.target)

		# Read the wikipedia page
		print('Parsing '+self.target+'\'s Wikipedia Page: '+page.url+" ...")
		parsed = parseWikiPage(page)

		print('Searching for Social Media links...')
		# Get social media links
		links = parsed['urls']
		sml = []
		nsml = []

		for l in links:
			link = NormalizeUrl(l)
			if isSocialMedia(link):
				sml.append({'url':link,'count':links[link]})
			else:
				nsml.append({'url':link,'count':links[link]})

		related = []
		recommandations = []
		
		print('Searching for recommandations...')
		for url in nsml:
			#url['url'] = NormalizeUrl(url['url'])
			sc = self.wikiUrlScope(url['url'])
			if sc['score'] > 0:
				related.append(url)
			elif sc['score'] == 0:
				recommandations.append({
					'target':UnquoteUrl(sc['target']),
					'url':url['url'],
					'count':url['count']
				})


		parsed['metadata']['external_links'] = sorted(recommandations,key=lambda x:x['count'],reverse=True)

		sml.extend(related)
		sml = sorted(sml,key=lambda x:x['count'],reverse=True)
		self.intel = {
			'target':self.target,
			'link': NormalizeUrl(page.url),
			'verified': False,
			'sm_links':sml,
			'metadata':parsed['metadata']
		}





def parseWikiPage(page):
	
	parsed = {
		'urls': Counted_UrlsFromHtml(page.html(),transform=True,base_url=urlparse(page.url)[1]),
		'metadata':{
			'title': page.title,
			'tags': page.categories,
		}
	}

	return parsed

	
def pageOf(target):
	return wiki.WikipediaPage(target)


def searchPage(target):
	# Get the Search List
	pages = wiki.search(target)
	return wiki.WikipediaPage(pages[0])




if __name__ == '__main__':

	import yaml

	WG = Wikipedia_Gatherer()
	t = tm.time()

	WG.gather(sys.argv[1])
	open('./Tests/WIKIPEDIA_Report_'+sys.argv[1].replace(' ','_')+'.yaml','w').write(yaml.dump(WG.get_report(),default_flow_style=False))
	print(str(int(tm.time()-t))+'s')
