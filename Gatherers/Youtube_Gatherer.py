from ..gatherer import Gatherer
from ..utils.TextFormatter import PureText,GetNumber
from ..utils.UrlExtractor import *


from bs4 import BeautifulSoup

import requests
import time as tm
import sys



class Youtube_Gatherer(Gatherer):
	"""docstring for Youtube_Gatherer"""
	
	def __init__(self, ):
		Gatherer.__init__(self)



	def extractRedirected(self,url):
		prs = ParseUrl(url)
		if prs[1] == '' and prs[2] == '/redirect':
			args = QueryArgs(prs[4])
			for arg in args:
				if arg[0] == 'q':
					return UnquoteUrl(arg[1])

		return url



	def gather_info(self):

		print('Searching for '+self.target+' on Youtube...')

		# Query Youtube search
		query = self.target.replace(' ','+')
		url = "https://www.youtube.com/results?sp=EgIQAg%253D%253D&search_query=" + query
		response = requests.get(url)
		soup = BeautifulSoup(response.content,'html.parser')

		# Take the first match
		yurl = 'https://www.youtube.com' + soup.find(class_='yt-uix-tile-link').get('href')

		urls = []
		if not yurl is '':
			soup = BeautifulSoup(requests.get(yurl+'/about').content,'html.parser')

			# Get Urls in description
			try:
				description = soup.find(class_='about-description').get_text()
			except:
				description = ''
			urls.extend(LocateUrlsInText(description))

			# Get Urls in Custom links
			custom_links = soup.findAll(class_='about-custom-links')
			for custom_link in custom_links:
				a = custom_link.findAll("a")
				for b in a:
					try:
						urls.append(b.get('href'))
					except:
						pass

			# Get Metadata
			metadata = {}
			# Country
			try:
				metadata['country'] = PureText(soup.find(class_='country-inline').get_text())
			except:
				pass
			#Stats
			for stat in soup.findAll(class_='about-stat'):
				s = stat.get_text()
				if 'subscribers' in s:
					metadata['followers'] = GetNumber(s,sep=',')
				elif 'views' in s:
					metadata['views'] = GetNumber(s,sep=',')

			# Verified
			verified = not soup.find(class_="yt-channel-title-icon-verified") is None



		# Regroup and normalize Links
		sml = []
		nsml = []
		for ul in urls:
			url = self.extractRedirected(ul)
			url = NormalizeUrl(url)
			if isSocialMedia(url):
				if not url in [u['url'] for u in sml]:
					sml.append({'url':url,'count':1})
				else:
					sml[[u['url'] for u in sml].index(url)]['count'] += 1
			else:
				if not url in [u['url'] for u in nsml]:
					nsml.append({'url':url,'count':1})
				else:
					nsml[[u['url'] for u in nsml].index(url)]['count'] += 1 


		metadata['external_links'] = nsml
		self.intel = {
			'target':self.target,
			'link':NormalizeUrl(yurl),
			'verified':verified,
			'sm_links':sml,
			'metadata':metadata
		}
			






if __name__ == '__main__':

	import yaml

	WG = Youtube_Gatherer()

	t = tm.time()

	WG.gather(sys.argv[1])
	open('./Tests/YOUTUBE_Report_'+sys.argv[1].replace(' ','_')+'.yaml','w').write(yaml.dump(WG.get_report(),default_flow_style=False))
	print(str(int(tm.time()-t))+'s')