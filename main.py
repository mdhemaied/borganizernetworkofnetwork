from .Borganizer import Borganizer
from .utils.CsvFiles import dictTocsv
from .Targeters.Facebook_Targeter import Facebook_Targeter

from collections import Counter
import yaml,os,time,sys
import platform


def Verify_Targetting_Method(tm):
	if not tm in TARGETTING_METHODS:
		return False
	return True

def Verify_Facebook_Initial_targets(ids):
	return True


TARGETTING_METHODS = ['facebook']

INITIAL_TARGETS_VERIFICATION_FUNCTION = {
	'facebook':Verify_Facebook_Initial_targets,
}

CONFIG_FILE_PATH = ''



if __name__ == '__main__':


	path_sep = '/'
	if platform.system() == 'Windows':
		path_sep = '\\'

	CONFIG_FILE_PATH = path_sep.join(['.','configs.yaml'])




	if len(sys.argv) < 3:
		print('Usage is: python main.py [TARGETTING_METHOD] [INITIAL_TARGETS] [OPTIONS]')
		exit(1)


	TargettingMethod = sys.argv[1].lower()
	if not Verify_Targetting_Method(TargettingMethod):
		print('Unknown Targetting Method.')
		print('Targetting Methods can be one of the following:')
		for tm in TARGETTING_METHODS:
			print('\t'+str(tm))
		exit(1)


	InitialTargets = sys.argv[2]
	if not INITIAL_TARGETS_VERIFICATION_FUNCTION[TargettingMethod](InitialTargets):
		print('Wrong Syntaxe for initial targets.')
		print('Initial Targets must be a list of valid targets (depending on the targetting method) separated by a coma.')
		exit(1)

	InitialTargets = sys.argv[2].split(',')

	for option in sys.argv[3:]:
		if len(option.split('=')) != 2:
			print('Options syntax is [Option_Name]=[Option_Value].')
			exit(1)

		option_name,option_value = option.split('=')
		if option_name == 'configs_file':
			CONFIG_FILE_PATH = option_value

		else:
			print('Unknown option '+str(option_name))
			exit(1)




	Borg = Borganizer()

	if TargettingMethod == 'facebook':
		Targ = Facebook_Targeter(cnf=CONFIG_FILE_PATH)
		Targ.init_WithTargetsID(InitialTargets)


	j = 1

	while True:

		i = 0

		dct = {
			'name':[],
			'fbid':[],
		}

		while i < 30:

			nxt = Targ.getNextTarget()
			print('\nChosen Target : '+str(nxt['id']+' - '+str(nxt['name'])))

			report = Borg.get_links_of({'name':str(nxt['name'])},method=Borg.BORGANIZER_SEARCH_METHODS)

			dct['name'].append(nxt['name'])
			dct['fbid'].append(nxt['id'])

			for site in report['sm_links']:
				if not site in dct.keys():
					dct[site] = []
				
				if TargettingMethod == 'facebook':
					if not site is 'facebook':
						dct[site].append(' , '.join([l for l in report['sm_links'][site]]))
					else:
						dct[site].append('facebook.com/'+str(nxt['id']))
				else:
					dct[site].append(' , '.join([l for l in report['sm_links'][site]]))

			i += 1

		directory = './Reports/FRIDAY_NIGHT_CSV'
		if not os.path.exists(directory):
			os.makedirs(directory)

		open(directory+'/'+str(j)+'.yaml','w').write(yaml.dump(dct,default_flow_style=False))
		dictTocsv(dct,directory+'/'+str(j)+'.csv')

		j +=1